# Installing MySQL on Windows 10 pro
## Windows Specs:
- Edition: Windows 10 Pro
- Version	20H2
- Installed on	‎03/‎08/‎2021
- OS build	19042.1165
- Experience	Windows Feature Experience Pack 120.2212.3530.0

# Introduction
MySQL is a well known open-source database management platform. You can install and run it as a server on your local windows 10 machine, which is particularly useful for development and testing.

# Getting Started
First of all you need to download the latest installer released by Oracle.

- https://dev.mysql.com/downloads/mysql/
- Then click on "Go to Download Page>"
- Here you have a choice of an installer wrapper (the top one), which will only download the bits you want to install, or a complete installer. Either one will work.
- Double click on the installer to run.
- On the first screen select the Developer Default setup type.
- There is a check to see if you want to link with other software. You can either download the necessary software (like Python), or just click through with next.
- If you are asked to select packages to install, unless you have a good reason to do otherwise just select the latest release of each of the items. 
- Click "Execute" from the download window to download the products necessary. This might take a while!
- On the next page click "Execute" to install all the packages.
- On the product configuration page you need to set up a mysql server on your machine to make any work with MySQL unless you intend to connect to an external MySQL for your development.
- Click through to the "Type and Networking" page.
- Select the Development Computer config type and accept the defaults.
- Use the default password option suggested, then select your password on the next screen.
- Assuming you want to use MySQL as a standard part of your Dev environment, make sure the "Start the MySQL Server at System Startup" is checked, then click next.
- Execute then finish on the following page, and you're done!
- You will be asked if you want to configure the router and the samples and examples. I left the router unconfigured and configured the sample and examples using the root login.
- On Finish, check the box to start the workbench.
- click on the Local Instance on the MySQL workbench homepage and you are going with a database at localhost:3306!

